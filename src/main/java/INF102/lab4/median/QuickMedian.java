package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {
    Random rnd = new Random();

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int half = list.size() / 2;

        return partition(listCopy, half);
    }

    public <T extends Comparable<T>> T partition(List<T> list, int half) {
        if (list.size() <= 1) {
            return list.get(0);
        }

        T pivot = list.get(half);

        List<T> left = new ArrayList<>();
        List<T> pivots = new ArrayList<>();
        List<T> right = new ArrayList<>();

        for (T element : list) {
            int cmp = element.compareTo(pivot);
            if (cmp < 0) {
                left.add(element);
            } else {
                if (cmp > 0) {
                    right.add(element);
                } else {
                    pivots.add(element);
                }
            }
        }

        if (half < left.size())
            return partition(left, half);
        else if (half < left.size() + pivots.size())
            return pivots.get(0);
        else
            return partition(right, half - left.size() - 1);
    }
}
// [1,4,3,2,9,5,6,8,7]

// p=7

// [1,4,3,2,5,6][7,8,9]

// Sletter listen til høyre siden 7 er på riktig

// [1,4,3,2,5,6]

// p=4

// [1,3,2][4][5,6]

// piv er nå indeks 3 som ikke er listen/2 derfor høyre

// [5,6]

// p=6

// [5][6]

// 6 er indeks 5, derfor ikke median.

// quickselect()-> Partition()
