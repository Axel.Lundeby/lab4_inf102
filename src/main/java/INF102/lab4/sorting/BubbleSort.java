package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();
        boolean swapped = true;

        while (swapped) {
            swapped = false;
            for (int i = 1; i < n; i++) {
                T nowElement = list.get(i - 1);
                T nextElement = list.get(i);
                if (nowElement.compareTo(nextElement) == 1) {
                    list.set(i, nowElement);
                    list.set(i - 1, nextElement);
                    swapped = true;
                }
            }
        }
    }

}
